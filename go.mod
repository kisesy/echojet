module gitea.com/kisesy/echojet

go 1.13

require (
	github.com/CloudyKit/jet/v3 v3.0.0-20200113104902-33cfc27b3e00
	github.com/labstack/echo/v4 v4.1.13
)
